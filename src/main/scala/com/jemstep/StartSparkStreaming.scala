package com.jemstep

import java.nio.file.{Path, WatchEvent}

import com.jemstep.services.FolderRegisterService._
import com.jemstep.services.{SocketService, WriterService}
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.streaming.{OutputMode, Trigger}

import scala.collection.JavaConverters._

object StartSparkStreaming {

  def main(args: Array[String]): Unit = {
    registerPath(AppConfig.directoryPath)
    monitorFileChanges()
    val sparkSession = createSparkSession()
    val readStream = sparkSession.readStream.format("socket")
      .option("host", AppConfig.socketHost)
      .option("port", AppConfig.socketPort)
      .load()
    //val writeStream = readStream.writeStream.outputMode("append").format("console").start()
    val writeStream = readStream.writeStream.outputMode(OutputMode.Append()).foreach(new WriterService).start()
    writeStream.awaitTermination()
  }

  def createSparkSession(): SparkSession = {
      SparkSession.builder()
        .appName(AppConfig.appName)
        .master("local[*]")
        .getOrCreate()
  }

  def monitorFileChanges(): Unit = {
    while(true) {
      val key = wathcerService.take()
      val events = key.pollEvents().asScala
      events.map(_ match {
        case x : WatchEvent[Path] => x
      }).foreach(event => {
        println("Path " + event.context().toAbsolutePath)
        println("Event " + event.kind)
        println("count " + event.count())
        val data = s"${event.context().toAbsolutePath.toString} ##  ${event.kind()} ##  ${event.count()}"
        SocketService.writeToSocket(data)
      })
      key.reset()
    }
  }

}
