package com.jemstep

import java.util.Properties

object AppConfig {

  val fileName = "application-dev.properties"
  val properteis = new Properties()
  properteis.load(getClass.getClassLoader.getResourceAsStream(fileName))
  val appName = properteis.getProperty("application.name")
  val directoryPath = properteis.getProperty("directory.path")
  val socketHost = properteis.getProperty("socket.host")
  val socketPort = properteis.getProperty("socket.port").toInt
}
