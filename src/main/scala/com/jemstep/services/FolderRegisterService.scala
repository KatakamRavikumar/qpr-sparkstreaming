package com.jemstep.services

import java.io.File
import java.nio.file.{FileSystems, Path, Paths}
import java.nio.file.StandardWatchEventKinds._
object FolderRegisterService {

  val wathcerService = FileSystems.getDefault.newWatchService()

  def registerPath(path : String): Unit = {
    val directory = Paths.get(path)
    directory.register(wathcerService, ENTRY_CREATE, ENTRY_MODIFY, ENTRY_DELETE)
    registerSubDirectories(path)
  }

  def registerSubDirectories(path : String): Unit = {
    val files = new File(path)
    files.listFiles().filter(file => file.isDirectory).foreach(path => {
      registerPath(path.getAbsolutePath)
      registerSubDirectories(path.getAbsolutePath)
    })
  }

}
