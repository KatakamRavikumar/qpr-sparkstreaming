package com.jemstep.services

import org.apache.spark.sql.{ForeachWriter, Row}

class WriterService extends ForeachWriter[Row] {
  override def open(partitionId: Long, epochId: Long): Boolean = {
    true
  }

  override def process(value: Row): Unit = {
    print("Recieved from socket through Streaming" + value.getString(0))
  }

  override def close(errorOrNull: Throwable): Unit = {
  }
}
