package com.jemstep.services

import java.io.{DataOutputStream, PrintWriter}
import java.net.{ServerSocket, Socket}

import com.jemstep.AppConfig

object SocketService {

  val serverSocket = new ServerSocket(AppConfig.socketPort)
  val clientSocket = new Socket(AppConfig.socketHost, AppConfig.socketPort)
  serverSocket.accept()
  val socketWriter = new PrintWriter(clientSocket.getOutputStream)
  def writeToSocket(data: String): Unit = {
    socketWriter.print(data)
    println(data)
  }
}
